let URL_PREFIX = 'https://cards-server-vk.herokuapp.com'
class api {
	async inital(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		return fetch(URL_PREFIX, {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getShop(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		return fetch(URL_PREFIX + '/shop', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async toggleCard(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/toggleCard', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async toggleFavoriteCard(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/toggleFavoriteCard', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getFavorites(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		return fetch(URL_PREFIX + '/getFavorites', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async sendImage(url, file, token, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		file.forEach(item => {
			fd.append('file[]', item)
		})
		fd.append('token', token)
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/upload', {
			method: 'POST',
			body: fd
		})
			.then((response) => response.text())
			.then((responseText) => {
				let resp = JSON.parse(responseText);
				return resp;
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getAuthorRating(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		return fetch(URL_PREFIX + '/authorRating', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async sendImageAdd(url, file, token, name, description) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('file', file)
		fd.append('token', token)
		fd.append('name', encodeURIComponent(name))
		fd.append('description', encodeURIComponent(description))
		return fetch(URL_PREFIX + '/uploadTask', {
			method: 'POST',
			body: fd
		})
			.then((response) => response.text())
			.then((responseText) => {
				let resp = JSON.parse(responseText);
				return resp;
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getAuthorWork(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		return fetch(URL_PREFIX + '/authorWork', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async deleteAuthorWork(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/deleteWork', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async buyShop(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/buy', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getCards(url, exist) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('exist', exist)
		return fetch(URL_PREFIX + '/getCards', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getUser(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/getUser', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getRating(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, encodeURIComponent(url[key]));
		}
		return fetch(URL_PREFIX + '/getRating', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
}

export default new api();
