import main from './main/reducer';
import vk from './vk/reducer';
import cards from './cards/reducer';
import rating from './rating/reducer';
import shop from './shop/reducer';
import img from './img/reducer';
import author from './author/reducer';
import favorites from './favorites/reducer'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { router5Middleware, router5Reducer } from 'redux-router5'
import thunk from 'redux-thunk'
import onActivateMiddleware from '../middleware/onActivate'
import routes from '../routes';

export default function configureStore(router, initialState = {}) {
    const createStoreWithMiddleware = applyMiddleware(
        thunk,
        router5Middleware(router),
        onActivateMiddleware(routes)
    )(createStore)

    const store = createStoreWithMiddleware(
        combineReducers({
            router: router5Reducer,
            vk,
            main,
            cards,
            rating,
            shop,
            img,
            author,
            favorites,
        }),
        initialState
    )

    window.store = store
    return store
}