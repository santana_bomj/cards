import * as types from './actionTypes';
import * as typesMain from '../main/actionTypes';
import * as actionMain from '../main/actions';
import api from '../../services/api';

export function openShop() {
    return async (dispatch, getState) => {
        try {
            if (!getState().shop.isInit) {
                let data = await api.getShop(getState().main.urlArray);
                if (data === undefined || data.status === 'ok') {
                    dispatch({ type: types.SET_INIT, shop: data.shop });
                }
            }
        } catch (error) {
            console.error(error);
        }
    }
}
export function buyShop(id, price) {
    return async (dispatch, getState) => {
        try {
            if (price > getState().main.balance) {
                dispatch(actionMain.setActiveModal('error', 'Не хватает баллов'))
                return
            }
            dispatch({ type: types.TOGGLE_LOADING, _id: id });
            let data = await api.buyShop(getState().main.urlArray, id)
            if (data === undefined || data.status === 'ok') {
                dispatch({ type: types.TOGGLE_LOADING, _id: id });
                dispatch({ type: typesMain.SET_DATA, data: { balance: data.data.balance, cards: getState().main.cards } })
                dispatch(actionMain.setActiveModal('true', 'Покупка совершена'))
            }
        } catch (error) {
            console.error(error);
        }
    }
}