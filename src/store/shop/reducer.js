import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    isInit: false,
    shop: []
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_INIT:
            return state.merge({
                shop: action.shop,
                isInit: true
            });
        case types.TOGGLE_LOADING:
            let temp_items = []
            state.shop.forEach(item => {
                if (item._id === action._id)
                    temp_items.push({ ...item, loading: !item.loading }) // need true
                else
                    temp_items.push({ ...item })
            })
            return state.merge({
                shop: temp_items
            });
        default:
            return state;
    }
}
export function getShop(state) {
    return state.shop.shop
}
export function getIsInit(state) {
    return state.shop.isInit
}