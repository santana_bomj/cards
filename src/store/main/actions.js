import React from 'react';
import * as types from './actionTypes';
import * as typesCards from '../cards/actionTypes';
import { actions } from 'redux-router5'
import api from '../../services/api';
import story from '../../services/story';
import bridge from '@vkontakte/vk-bridge';
import html2canvas from 'html2canvas';

import { PopoutWrapper, ModalCard } from '@vkontakte/vkui';
import Icon56ErrorOutline from '@vkontakte/icons/dist/56/error_outline';

function parseQueryString(string) {
    return string.split('&')
        .map((queryParam) => {
            let kvp = queryParam.split('=');
            return { key: kvp[0], value: kvp[1] }
        })
        .reduce((query, kvp) => {
            query[kvp.key] = kvp.value;
            return query
        }, {})
};

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function goMain() {
    return async (dispatch, getState) => {
        try {
            dispatch(actions.navigateTo('main', {}, { replace: true }))
        } catch (error) {
            console.error(error);
        }
    }
}

export function initalMain() {
    return async (dispatch, getState) => {
        try {
            let name = ''
            let avatar = ''
            await bridge
                .sendPromise('VKWebAppGetUserInfo')
                .then(data => {
                    name = data.first_name + ' ' + data.last_name.slice(0, 1) + '.'
                    avatar = data.photo_100
                    dispatch({ type: types.SET_AVATAR, value: avatar });
                    dispatch({ type: types.SET_NAME, value: name });
                })
                .catch(error => {
                    console.log(error)
                });
            let url = getState().main.url + '&hash=' + decodeURIComponent(decodeURIComponent(window.location.hash)) + '&user_name=' + encodeURIComponent(name) + '&avatar=' + avatar
            dispatch({ type: types.SET_URL, urlArray: parseQueryString(url), url });
            let data = await api.inital(getState().main.urlArray);
            if (data === undefined || data.status === 'error')
                dispatch(setPopout(<PopoutWrapper className='ModalCard__reconnect'>
                    <ModalCard
                        header='Подключение прервано'
                        caption='Обнаружены неполадки с соединением'
                        icon={<Icon56ErrorOutline />}
                        onClose={() => { }}
                        actions={[{
                            title: 'Переподключиться',
                            mode: 'primary',
                            action: () => {
                                dispatch(actions.navigateTo('start', { s: '&' + getState().main.url }, { replace: true }))
                                setTimeout(() => window.location.reload(), 1000)
                            }
                        }]}
                    >
                    </ModalCard>
                </PopoutWrapper>
                ))
            else if (data.status === 'ok') {
                dispatch({ type: typesCards.SET_DATA, data: data.cards })
                dispatch({ type: types.SET_DATA, data: data.data })
                dispatch({ type: types.SET_PERFORMED_CARDS, data: data.performedCards })
                if (!data.newUser)
                    dispatch(goMain())
                else dispatch({ type: types.SET_NEW_USER })
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function setPopout(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_POPOUT, value: value });
        } catch (error) {
            console.error(error);
        }
    };
}

export function closePopout() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLOSE_POPOUT });
        } catch (error) {
            console.error(error);
        }
    };
}

export function setSnackbar(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_SNACKBAR, value: value });
        } catch (error) {
            console.error(error);
        }
    };
}

export function closeSnackbar() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLOSE_SNACKBAR });
        } catch (error) {
            console.error(error);
        }
    };
}

export function modalBack(value = 1) {
    return async (dispatch, getState) => {
        try {
            window.history.go(value * -1)
            dispatch(setActiveModal(getState().main.modalHistory[getState().main.modalHistory.length - 2]))
        } catch (error) {
            console.error(error);
        }
    };
}

export function setActiveModal(activeModal, msg = '') {
    return async (dispatch, getState) => {
        try {
            activeModal = activeModal || null;
            let modalHistory = getState().main.modalHistory ? [...getState().main.modalHistory] : [];

            if (activeModal === null) {
                modalHistory = [];
            } else if (modalHistory.indexOf(activeModal) !== -1) {
                modalHistory = modalHistory.splice(0, modalHistory.indexOf(activeModal) + 1);
            } else {
                modalHistory.push(activeModal);
            }
            dispatch({ type: types.SET_ACTIVE_MODAL, activeModal, modalHistory, msg });
        } catch (error) {
            console.error(error);
        }
    };
}

export function getUser(params) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_ACTIVE_USER_LOADIND, value: true });
            let data = await api.getUser(getState().main.urlArray, params.id);
            dispatch({ type: types.SET_ACTIVE_USER, data: data.user });
        } catch (error) {
            console.error(error);
        }
    };
}

export function getUserOpen(params) {
    return async (dispatch, getState) => {
        try {
            dispatch(actions.navigateTo('profileuser', params))
        } catch (error) {
            console.error(error);
        }
    };
}


export function goBack() {
    return async (dispatch, getState) => {
        try {
            if (getState().main.activeModal !== null) {
                dispatch({ type: types.SET_ACTIVE_MODAL, activeModal: null });
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function setPath() {
    return async (dispatch, getState) => {
        try {
            dispatch(actions.navigateTo(getState().router.route.name))
        } catch (error) {
            console.error(error);
        }
    };
}

export function shareBtn(item = '') {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_ACTIVE_MODAL, activeModal: 'share' });
            if (item === 'profile') {
                dispatch({ type: types.SET_CANVAS_TYPE, value: 'profile' });
                setTimeout(() => {
                    if (document.getElementById('Card__profile'))
                        html2canvas(document.getElementById('Card__profile'), { imageTimeout: 0, allowTaint: true, useCORS: true, scrollX: scrollbarWidth() === 0 ? 0 : -10, scrollY: -window.scrollY, scale: 5 }).then(item => {
                            dispatch({ type: types.SET_CANVAS, value: item.toDataURL() });
                        })
                }, 300)
            } else if (item !== '') {
                dispatch({ type: types.SET_CANVAS_TYPE, value: 'card' });
                setTimeout(() => {
                    if (document.getElementById(item))
                        html2canvas(document.getElementById(item), { imageTimeout: 0, allowTaint: true, useCORS: true, scrollX: scrollbarWidth() === 0 ? 0 : -10, scrollY: -window.scrollY, scale: 5 }).then(item => {
                            dispatch({ type: types.SET_CANVAS, value: item.toDataURL() });
                        })
                }, 300)
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function share() {
    return async (dispatch, getState) => {
        try {
            let stories
            if (getState().main.canvas) {
                stories = story.getProfile(getState().main.canvas)
                bridge.send("VKWebAppShowStoryBox", stories)
            }
        } catch (error) {
            console.error(error);
        }
    };
}

function scrollbarWidth() {
    var documentWidth = parseInt(document.documentElement.clientWidth);
    var windowsWidth = parseInt(window.innerWidth);
    var scrollbarWidth = windowsWidth - documentWidth;
    return scrollbarWidth;
}