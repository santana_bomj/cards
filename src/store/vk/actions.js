import React from 'react'
import bridge from '@vkontakte/vk-bridge';
import { Snackbar, Avatar } from '@vkontakte/vkui';
import Icon16Done from '@vkontakte/icons/dist/16/done';
import * as types from './actionTypes';
import * as mainActions from '../main/actions'

export function initApp() {
    return async (dispatch) => {
        bridge.subscribe(e => {
            let vkEvent = e.detail;
            if (!vkEvent) {
                console.error('invalid event', e);
                return;
            }
            let type = vkEvent['type'];
            let data = vkEvent['data'];
            switch (type) {
                case 'VKWebAppUpdateConfig':
                    document.body.setAttribute('scheme', data.scheme)
                    break;
                case 'VKWebAppShowStoryBoxLoadFinish':
                    window.history.go(-1)
                    dispatch(mainActions.setSnackbar(<Snackbar
                        layout="vertical"
                        onClose={() => dispatch(mainActions.closeSnackbar())}
                        before={<Avatar size={24} style={{ backgroundColor: 'var(--accent)' }}><Icon16Done fill="#fff" width={14} height={14} /></Avatar>}
                    >
                        История опубликована
              </Snackbar>))
                    break
                default:
            }
        });

        bridge.send('VKWebAppInit', {});
        bridge.send('VKWebAppUpdateConfig', {});
    }
}

export function VKWebAppGetAuthToken(scope) {
    return async (dispatch, getState) => {
        try {
            await bridge
                .sendPromise('VKWebAppGetAuthToken', { "app_id": 7380525, "scope": scope })
                .then(data => {
                    dispatch({ type: types.VK_SET_TOKEN, vk_access_token_settings: data.scope, vk_access_token: data.access_token })
                })
                .catch(error => {
                    console.log(error)
                });
        } catch (error) {
            console.error(error);
        }
    };
}