export const SET_WORK_LOADED = 'author.SET_WORK_LOADED';
export const SET_USER_CARDS = 'author.SET_USER_CARDS';
export const SET_AUTHOR_LOADING = 'author.SET_AUTHOR_LOADING';
export const SET_AUTHOR_RATING = 'author.SET_AUTHOR_RATING';