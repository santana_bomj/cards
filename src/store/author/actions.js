import * as types from './actionTypes';
import api from '../../services/api';

export function openAuthor() {
    return async (dispatch, getState) => {
        try {
            if (getState().author.author_loaded)
                return
            dispatch({ type: types.SET_AUTHOR_LOADING, value: true });
            const data = await api.getAuthorRating(getState().main.urlArray);
            if (data.status === 'ok' && data.data !== undefined) {
                dispatch({ type: types.SET_AUTHOR_RATING, data: data.data });
            }
        } catch (error) {
            console.error(error);
        }
    }
}

export function openWork() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_WORK_LOADED, data: { value: true } });
            const data = await api.getAuthorWork(getState().main.urlArray);
            dispatch({ type: types.SET_USER_CARDS, cards: data.data })
        } catch (error) {
            console.error(error);
        }
    }
}

export function deleteCard(id) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_WORK_LOADED, data: { value: true } });
            const data = await api.deleteAuthorWork(getState().main.urlArray, id);
            dispatch({ type: types.SET_USER_CARDS, cards: data.data })
        } catch (error) {
            console.error(error);
        }
    }
}