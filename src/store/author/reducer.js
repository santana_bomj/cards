import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    author_loading: false,
    author_loaded: false,
    author_rating: [],
    work_loaded: false,
    user_cards: [],
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_AUTHOR_LOADING:
            return state.merge({
                author_loading: action.value
            });
        case types.SET_WORK_LOADED:
            return state.merge({
                work_loaded: action.data.value
            });
        case types.SET_USER_CARDS:
            return state.merge({
                user_cards: action.cards,
                work_loaded: false,
            });
        case types.SET_AUTHOR_RATING:
            return state.merge({
                author_rating: action.data,
                author_loading: false,
                author_loaded: true
            });
        default:
            return state;
    }
}
export function getAuthorLoading(state) {
    return state.author.author_loading
}
export function getAuthorRating(state) {
    return state.author.author_rating
}
export function getWorkLoaded(state) {
    return state.author.work_loaded
}
export function getUserCards(state) {
    return state.author.user_cards
}