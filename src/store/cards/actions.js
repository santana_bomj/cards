import * as types from './actionTypes';
import * as typesFavorites from '../favorites/actionTypes';
import api from '../../services/api';

export function swipeCards(id) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_HIDE, id: id });
            if (getState().cards.data.filter(item => item.show).length < 5)
                dispatch(loadCards())
        } catch (error) {
            console.error(error);
        }
    }
}

export function toggleCard(id) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.LIKE_TOGGLE, id: id });
            api.toggleCard(getState().main.urlArray, id);
            dispatch({ type: typesFavorites.SET_INIT_FALSE })
        } catch (error) {
            console.error(error);
        }
    }
}

export function loadCards() {
    return async (dispatch, getState) => {
        try {
            console.log('loadCards', getState().cards.hide)
            let data = await api.getCards(getState().main.urlArray, JSON.stringify([...getState().cards.hide, ...getState().cards.data.map(item => item._id)]));
            console.log(data)
            if (data.status === 'ok' && data.cards.length === 0)
                dispatch({ type: types.SET_EMPTY });
            else if (data.status === 'ok')
                dispatch({ type: types.APPEND_DATA, data: data.cards })
        } catch (error) {
            console.error(error);
        }
    }
}

export function clearHide() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLEAR_EMPTY });
            dispatch(loadCards())
        } catch (error) {
            console.error(error);
        }
    }
}