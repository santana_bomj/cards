import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    data: null,
    hide: [],
    empty: false,
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_DATA:
            let data = action.data.map(item => {
                return { ...item, show: true, likeState: false }
            })
            return state.merge({
                data
            });
        case types.APPEND_DATA:
            let append_data = action.data.map(item => {
                return { ...item, show: true, likeState: false }
            })
            let temp = [...append_data, ...state.data]
            return state.merge({
                data: temp,
                loading: false
            });
        case types.LIKE_TOGGLE:
            let temp_like = []
            state.data.forEach(item => {
                if (item._id === action.id)
                    temp_like.push({ ...item, likeState: !item.likeState })
                else
                    temp_like.push({ ...item })
            })
            return state.merge({
                data: temp_like,
            });
        case types.SET_HIDE:
            let temp_items = []
            let hide = []
            state.data.forEach(item => {
                if (item._id !== action.id)
                    temp_items.push({ ...item })
                else
                    hide.push(item._id)
            })
            return state.merge({
                data: temp_items,
                hide: [...state.hide, ...hide]
            });
        case types.SET_EMPTY:
            return state.merge({
                empty: true
            });
        case types.CLEAR_EMPTY:
            return state.merge({
                empty: false,
                hide: []
            })
        default:
            return state;
    }
}
export function getData(state) {
    return state.cards.data
}
export function getEmpty(state) {
    return state.cards.empty
}