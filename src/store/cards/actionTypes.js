export const SET_DATA = 'cards.SET_DATA';
export const APPEND_DATA = 'cards.APPEND_DATA';
export const SET_HIDE = 'cards.SET_HIDE';
export const SET_EMPTY = 'cards.SET_EMPTY';
export const LIKE_TOGGLE = 'cards.LIKE_TOGGLE';
export const CLEAR_EMPTY = 'cards.CLEAR_EMPTY';