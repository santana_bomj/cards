import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    loaded: false,
    data: []
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_DATA:
            return state.merge({
                loaded: true,
                data: action.data
            });
        case types.SET_LOADED:
            return state.merge({
                loaded: action.data.value
            });
        default:
            return state;
    }
}
export function getLoaded(state) {
    return state.rating.loaded
}
export function getData(state) {
    return state.rating.data
}