import * as types from './actionTypes';
import api from '../../services/api';

export function setData() {
    return async (dispatch, getState) => {
        try {
            if (getState().rating.loaded)
                return
            let data = await api.getRating(getState().main.urlArray);
            if (data.status === 'ok')
                dispatch({ type: types.SET_DATA, data: data.data })
        } catch (error) {
            console.error(error);
        }
    }
}