import React from 'react'
import * as types from './actionTypes';
import api from '../../services/api';
import * as vkActions from '../vk/actions'
import * as mainActions from '../main/actions'
import * as authorActions from '../author/actions'
import { ScreenSpinner } from '@vkontakte/vkui';

export function onCropComplete(crop) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_CROPPED_IMAGE, crop })
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function onSelectFile(e) {
    return async (dispatch, getState) => {
        try {
            if (e.target.files && e.target.files.length > 0) {
                const reader = new FileReader();
                reader.addEventListener('load', () =>
                    dispatch({ type: types.SET_SELECT_FILE, src: reader.result })
                );
                reader.readAsDataURL(e.target.files[0]);
            }
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function onCropChange(crop, percentCrop) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_CROP, crop });
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function toggleSelected() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_SELECTED });
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function sendImage() {
    return async (dispatch, getState) => {
        try {
            dispatch(mainActions.setPopout(<ScreenSpinner />))
            if (getState().vk.vk_access_token === '')
                await dispatch(vkActions.VKWebAppGetAuthToken('photos'))
            let data = await api.sendImageAdd(getState().main.urlArray, getState().img.cropped_image, getState().vk.vk_access_token, getState().img.name, getState().img.description);
            dispatch(mainActions.closePopout())
            if (data.status === 'ok') {
                dispatch({ type: 'main.SET_MSG', true: true, msg: 'Задание загружено и отправлено на модерацию' });
                dispatch({ type: types.SET_DEFAULT });
                dispatch(authorActions.openWork())
            } else {
                dispatch({ type: 'main.SET_MSG', msg: data.message ? data.message : 'Попробуйте ещё раз' });
            }
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function onGroupCropChange(crop, percentCrop) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_GROUP_CROP, crop });
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function handleInput(event) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.HANDLE_INPUT, name: event.target.name, value: event.target.value });
        } catch (error) {
            console.error(error);
        }
    };
}