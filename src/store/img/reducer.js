import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    crop: { unit: '%', width: 30, aspect: 3 / 4, },
    file: '',
    cropped_image: null,
    selected: false,
    cropGroup: { unit: '%', width: 30, aspect: 3 / 4, },
    fileGroup: '',
    name: '',
    description: '',
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_CROP:
            return state.merge({
                crop: action.crop
            })
        case types.SET_SELECT_FILE:
            return state.merge({
                file: action.src
            })
        case types.SET_CROPPED_IMAGE:
            return state.merge({
                cropped_image: action.crop
            })
        case types.SET_SELECTED:
            return state.merge({
                selected: !state.selected
            })
        case types.SET_DEFAULT:
            return state.merge({
                file: '',
                cropped_image: null,
                selected: false,
                name: '',
                description: ''
            })
        case types.SET_GROUP_CROP:
            return state.merge({
                cropGroup: action.crop
            })
        case types.HANDLE_INPUT:
            return state.merge({
                [action.name]: action.value
            })
        default:
            return state;
    }
}
export function getCrop(state) {
    return state.img.crop
}
export function getFile(state) {
    return state.img.file
}
export function getCroppedImage(state) {
    return state.img.cropped_image
}
export function getSelected(state) {
    return state.img.selected
}
export function getGroupCrop(state) {
    return state.img.cropGroup
}
export function getGroupFile(state) {
    return state.img.fileGroup
}
export function getName(state) {
    return state.img.name
}
export function getDescription(state) {
    return state.img.description
}