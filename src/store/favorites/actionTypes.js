export const SET_INIT = 'favorites.SET_INIT';
export const SET_LOADING = 'favorites.SET_LOADING';
export const APPEND_SELECT_FILE = 'favorites.APPEND_SELECT_FILE'
export const REMOVE_FILE = 'favorites.REMOVE_FILE'
export const SET_DEFAULT = 'favorites.SET_DEFAULT'
export const SET_ACTIVE_FAVORITE = 'favorites.SET_ACTIVE_FAVORITE'
export const LIKE_TOGGLE = 'favorites.LIKE_TOGGLE'
export const SET_INIT_FALSE = 'favorites.SET_INIT_FALSE'