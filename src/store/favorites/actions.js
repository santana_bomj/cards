import React from 'react';
import * as types from './actionTypes';
import api from '../../services/api';
import { ScreenSpinner } from '@vkontakte/vkui'
import * as mainActions from '../main/actions'

export function openFavorites() {
    return async (dispatch, getState) => {
        try {
            if (!getState().favorites.isInit) {
                let data = await api.getFavorites(getState().main.urlArray);
                if (data === undefined || data.status === 'ok') {
                    dispatch({ type: types.SET_INIT, data: data.data });
                }
            }
        } catch (error) {
            console.error(error);
        }
    }
}
export function openFavorite(_id) {
    return async (dispatch, getState) => {
        try {
            if (_id)
                dispatch({ type: types.SET_ACTIVE_FAVORITE, _id });
        } catch (error) {
            console.error(error);
        }
    }
}

export function onSelectFile(e) {
    return async (dispatch, getState) => {
        try {
            if (e.target.files && e.target.files.length > 0) {
                for (let i = 0; i < e.target.files.length; i++) {
                    const reader = new FileReader();
                    reader.addEventListener('load', () =>
                        dispatch({ type: types.APPEND_SELECT_FILE, data: reader.result })
                    );
                    reader.readAsDataURL(e.target.files[i]);
                }
                e.target.value = ''
            }
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function removeFile(data) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.REMOVE_FILE, data })
        } catch (error) {
            console.error(error);
        }
    }
}

export function sendImage() {
    return async (dispatch, getState) => {
        try {
            dispatch(mainActions.setPopout(<ScreenSpinner />))
            let data = await api.sendImage(getState().main.urlArray, getState().favorites.img, getState().vk.vk_access_token, getState().favorites.active._id);
            dispatch(mainActions.closePopout())
            if (data.status === 'ok') {
                dispatch({ type: 'main.SET_MSG', true: true, msg: 'Сохранено и отправлено на модерацию' });
                dispatch({ type: types.SET_DEFAULT });
            } else {
                dispatch({ type: 'main.SET_MSG', msg: data.message ? data.message : 'Не удалось загрузить, попробуйте ещё раз' });
            }
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function toggleCard(id) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.LIKE_TOGGLE, id: id });
            api.toggleFavoriteCard(getState().main.urlArray, id);
            dispatch({ type: types.SET_INIT_FALSE })
        } catch (error) {
            console.error(error);
        }
    }
}