import * as types from './actionTypes';

const initialState = {
    isInit: false,
    data: [],
    img: [],
    active: {}
};

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_INIT:
            return { ...state, data: action.data, isInit: true }
        case types.SET_INIT_FALSE:
            return { ...state, isInit: false }
        case types.APPEND_SELECT_FILE:
            return { ...state, img: [...state.img, action.data] }
        case types.REMOVE_FILE:
            return { ...state, img: state.img.filter(item => item !== action.data) }
        case types.SET_DEFAULT:
            return { ...state, data: [], isInit: false }
        case types.SET_ACTIVE_FAVORITE:
            let active = {}
            let img = state.img
            state.data.forEach(item => {
                if (item._id === action._id) {
                    active = item
                    if (item.attachments.length > 0)
                        img = [...item.attachments]
                }
            })
            return { ...state, active, img }
        case types.LIKE_TOGGLE:
            let temp_like = []
            state.data.forEach(item => {
                if (item._id === action.id)
                    temp_like.push({ ...item, likeState: !item.likeState })
                else
                    temp_like.push({ ...item })
            })
            return { ...state, data: temp_like }
        default:
            return state;
    }
}
export function getFavorites(state) {
    return state.favorites.data
}
export function getIsInit(state) {
    return state.favorites.isInit
}
export function getImg(state) {
    return state.favorites.img
}
export function getActive(state) {
    return state.favorites.active
}