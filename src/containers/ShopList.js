import React from 'react';
import { Cell, Spinner } from '@vkontakte/vkui';
import Icon28AddOutline from '@vkontakte/icons/dist/28/add_outline';

function ShopList(props) {
        let padding = { padding: 2 }
        let shopElements
        if (props.data.length > 0) {
                shopElements = props.data.map(item => {
                        let price = item.price;
                        return <Cell
                                key={item._id}
                                multiline
                                indicator={price}
                                description={item.description}
                                asideContent={(item.loading ? <Spinner size="regular" style={padding} /> : <Icon28AddOutline className={props.balance >= price ? 'Accent' : ''} onClick={() => props.buyShop(item._id, price)} />)}>
                                {item.name}
                        </Cell>
                })
        }
        return (
                <>
                        {shopElements}
                </>
        )

}

export default ShopList;