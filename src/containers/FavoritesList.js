import React from 'react';
import { Group, FormStatus, Div, Button, Placeholder } from '@vkontakte/vkui';
import Icon24Like from '@vkontakte/icons/dist/24/like';
import Icon24LikeOutline from '@vkontakte/icons/dist/24/like_outline';
import Icon24ShareOutline from '@vkontakte/icons/dist/24/share_outline';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';
import Icon56FavoriteOutline from '@vkontakte/icons/dist/56/favorite_outline';

function FavoritesList(props) {
        let favoritesElements = <Placeholder
                icon={<Icon56FavoriteOutline />}
                stretched
        >
                У Вас нет избранных заданий
        </Placeholder>
        if (props.data.length !== 0) {
                favoritesElements = props.data.map(item => {
                        return <Group key={item._id}>
                                {!item.moderated && item.sent && <FormStatus className='FormStatus__margin'>
                                        Находится на модерации
                                                </FormStatus>}
                                {item.message_out !== undefined && item.message_out !== '' && <FormStatus header='Ответ от модератора' className='FormStatus__margin'>
                                        {item.message_out}
                                </FormStatus>}
                                <div className='Cards'>
                                        <div className='Cards__container'>
                                                <div className='Cards__item' style={{ backgroundImage: `linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7) ),url(${item.src})` }}>
                                                        <div className='Cards__item_container'>
                                                                <div className='Cards__item_title'>{decodeURIComponent(item.name)}</div>
                                                                <div className='Cards__item_subtitle'>{decodeURIComponent(item.description.slice(0, 256)) + (item.description.length >= 256 ? '...' : '')}</div>
                                                                <div className='Cards__item_bottom'>
                                                                        <div className='Cards__item_icon' onClick={() => props.toggleCard(item._id)}>{item.likeState ? <Icon24Like /> : <Icon24LikeOutline />}</div>
                                                                        <div className='Cards__item_icon'><span>{item.cost}</span><Icon24FavoriteOutline /></div>
                                                                        <div className='Cards__item_icon'><Icon24ShareOutline /></div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <Div>
                                        <Button size='xl' onClick={() => props.router.navigate('addPerformed', { _id: item._id })}>
                                                {item.sent ? 'Редактировать' : 'Подтвердить выполнение'}
                                        </Button>
                                </Div>
                        </Group>
                })
        }
        return (
                <>
                        {favoritesElements}
                </>
        )

}

export default FavoritesList;