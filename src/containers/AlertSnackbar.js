import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Snackbar, Avatar } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import { closeSnackbar} from "../store/main/actions";
import Icon16Done from '@vkontakte/icons/dist/16/done';

class AlertSnackbar extends Component {
    render() {
        const blueBackground = {
            backgroundColor: 'var(--accent)'
          };
        return (
            this.props.type === 'accept' ?
                <Snackbar
                    layout="vertical"
                    onClose={() => this.props.dispatch(closeSnackbar())}
                    before={<Avatar size={24} style={blueBackground}><Icon16Done fill="#fff" width={14} height={14} /></Avatar>}
                >
                    {this.props.title}
      </Snackbar>
                : null
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

export default connect(mapStateToProps)(AlertSnackbar);
