import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ConfigProvider, Epic, Tabbar, TabbarItem, View } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from '../store/main/reducer';
import * as vkSelectors from '../store/vk/reducer';
import * as vkActions from '../store/vk/actions';
import * as mainActions from '../store/main/actions';
import StartPanel from './StartPanel';
import MainPanel from './MainPanel';
import ProfilePanel from './ProfilePanel';
import FavoritesPanel from './FavoritesPanel';
import AuthorPanel from './AuthorPanel';
import RatingPanel from './RatingPanel';
import ProfileUserPanel from './ProfileUserPanel';
import AddPanel from './AddPanel';
import AddPerformedPanel from './AddPerformedPanel';
import WorkPanel from './WorkPanel';
import ShopPanel from './ShopPanel';
import { RouteNode } from 'react-router5'
import Icon28Profile from '@vkontakte/icons/dist/28/profile';
import Icon28FavoriteOutline from '@vkontakte/icons/dist/28/favorite_outline';
import Icon28DevicesOutline from '@vkontakte/icons/dist/28/devices_outline';
import Icon28MarketOutline from '@vkontakte/icons/dist/28/market_outline';
import Icon28FireOutline from '@vkontakte/icons/dist/28/fire_outline';

import ModalContainer from './Modals/ModalContainer';

class App extends Component {

    componentDidMount() {
        this.props.dispatch(vkActions.initApp());
        this.props.dispatch(mainActions.initalMain());
        window.addEventListener('popstate', e => { e.preventDefault(); this.props.dispatch(mainActions.goBack()); });
    }

    render() {
        let activePanel = 'main'
        let activeStory
        let tabbar
        switch (this.props.route.name) {
            case 'start':
                activeStory = 'start'
                tabbar = null
                break
            case 'main':
                activeStory = 'main'
                break
            case 'achievements':
                activeStory = 'profile'
                activePanel = 'achievements'
                break;
            case 'profile':
                activeStory = 'profile'
                break
            case 'profileuser':
                activePanel = 'user'
                activeStory = 'profile'
                break
            case 'like':
                activeStory = 'main'
                activePanel = 'like'
                break
            case 'rating':
                activeStory = 'rating'
                break
            case 'add':
                activePanel = 'add'
                activeStory = 'author'
                break
            case 'addPerformed':
                activePanel = 'addPerformed'
                activeStory = 'main'
                break
            case 'work':
                activePanel = 'work'
                activeStory = 'author'
                break
            case 'author':
                activeStory = 'author'
                break
            case 'shop':
                activeStory = 'shop'
                break
            default:
                activeStory = 'start'
                break
        }

        if (tabbar !== null)
            tabbar = <Tabbar>
                <TabbarItem
                    selected={activeStory === 'profile'}
                    onClick={() => {
                        this.props.router.navigate('profile', {}, { replace: true })
                    }}><Icon28Profile /></TabbarItem>
                <TabbarItem
                    selected={activeStory === 'shop'}
                    onClick={() => {
                        this.props.router.navigate('shop', {}, { replace: true })
                    }}><Icon28MarketOutline /></TabbarItem>
                <TabbarItem
                    onClick={() => {
                        this.props.router.navigate('main', {}, { replace: true })
                    }}
                    selected={activeStory === 'main'}
                ><Icon28DevicesOutline /></TabbarItem>
                <TabbarItem
                    onClick={() => {
                        this.props.router.navigate('author', {}, { replace: true })
                    }}
                    selected={activeStory === 'author'}><Icon28FireOutline /></TabbarItem>
                <TabbarItem
                    onClick={() => {
                        this.props.router.navigate('rating', {}, { replace: true })
                    }}
                    selected={activeStory === 'rating'}><Icon28FavoriteOutline /></TabbarItem>
            </Tabbar>

        return (
            <ConfigProvider insets={this.props.insets} >
                <Epic activeStory={activeStory} tabbar={tabbar}>
                    <View id="start" activePanel="main" popout={this.props.popout} modal={<ModalContainer />}>
                        <StartPanel router={this.props.router} id="main" />
                    </View>
                    <View id="main" activePanel={activePanel} popout={this.props.popout} modal={<ModalContainer />}>
                        <MainPanel router={this.props.router} id="main" />
                        <FavoritesPanel router={this.props.router} id="like" />
                        <AddPerformedPanel router={this.props.router} id="addPerformed" />
                    </View>
                    <View id="profile" activePanel={activePanel} popout={this.props.popout} modal={<ModalContainer name={this.props.route.name} />}>
                        <ProfilePanel router={this.props.router} route={this.props.route} id="main" />
                        <ProfileUserPanel router={this.props.router} id="user" />
                    </View>
                    <View id="shop" activePanel='main' popout={this.props.popout} modal={<ModalContainer />}>
                        <ShopPanel router={this.props.router} id="main" />
                    </View>
                    <View id="author" activePanel={activePanel} popout={this.props.popout} modal={<ModalContainer />}>
                        <AuthorPanel router={this.props.router} id="main" />
                        <AddPanel router={this.props.router} id="add" />
                        <WorkPanel router={this.props.router} id="work" />
                    </View>
                    <View id="rating" activePanel="main" popout={this.props.popout}>
                        <RatingPanel router={this.props.router} id="main" />
                    </View>
                </Epic>
            </ConfigProvider>
        );
    }
}

function mapStateToProps(state) {
    return {
        popout: mainSelectors.getPopout(state),
        insets: vkSelectors.getInsets(state),
    };
}

export default connect(mapStateToProps)(
    (props) => (
        <RouteNode nodeName="">
            {({ route }) => <App route={route} {...props} />}
        </RouteNode>
    )
);
