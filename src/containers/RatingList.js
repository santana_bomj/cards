import React from 'react';
import { Group, Cell, List, Avatar } from '@vkontakte/vkui';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';

function RatingList(props) {
        let ratingElements
        if (props.data.length !== 0) {
                ratingElements = props.data.map(item => {
                        return <React.Fragment key={item._id}>
                                <Cell onClick={item._id ? () => props.getUserOpen({ id: item._id }) : () => { }} before={<Avatar size={40} src={item.avatar} />} indicator={<div className='Balance'><span>{item.rating}</span><Icon24FavoriteOutline /></div>} >{item.name}</Cell>
                        </React.Fragment>
                })
        }
        return (
                <Group>
                        <List>
                                {ratingElements}
                        </List>
                </Group>
        )

}

export default RatingList;