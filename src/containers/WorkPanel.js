import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, PanelHeaderBack, Spinner } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

import * as mainActions from '../store/main/actions';
import * as authorSelectors from "../store/author/reducer";
import * as authorActions from '../store/author/actions';

import WorkList from './WorkList'

class WorkPanel extends Component {
    render() {
        console.log(this.props.user_cards)
        return (
            <Panel id={this.props.id}>
                <PanelHeader left={<PanelHeaderBack onClick={() => window.history.go(-1)} />}>
                    Мои крокодилы
                </PanelHeader>
                {this.props.work_loaded ?
                    <div className='Spinner__margin'><Spinner size="regular" /></div> :
                    <WorkList deleteCard={this.props.deleteCard} setPopout={this.props.setPopout} user_cards={this.props.user_cards} />
                }
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        work_loaded: authorSelectors.getWorkLoaded(state),
        user_cards: authorSelectors.getUserCards(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setPopout: (value) => dispatch(mainActions.setPopout(value)),
        deleteCard: (id) => dispatch(authorActions.deleteCard(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkPanel);
