import React from 'react';
import { Spinner, Placeholder, Button } from '@vkontakte/vkui';
import { Spring, animated } from 'react-spring'
import { Gesture } from 'react-with-gesture'
import { useDoubleTap } from "use-double-tap";
import "./Cards.scss";

import Icon24Like from '@vkontakte/icons/dist/24/like';
import Icon24LikeOutline from '@vkontakte/icons/dist/24/like_outline';
import Icon24ShareOutline from '@vkontakte/icons/dist/24/share_outline';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';
import Icon56CheckCircleDeviceOutline from '@vkontakte/icons/dist/56/check_circle_device_outline';

function calculateX(index, down, xDir, xDelta, gone) {
  if (gone) {
    return (200 + window.innerWidth) * (xDir < 0 ? -1 : 1)
  } else {
    return down ? xDelta : 0
  }
}

function Cards(props) {
  let cardsElements = null
  let bind = useDoubleTap((e) => {
    props.toggleCard(e.target.id);
  });
  if (props.cards !== undefined && props.cards !== null)
    cardsElements = props.cards.map(
      card =>
        card.show && (
          <Gesture key={card._id}>
            {({ down, delta: [xDelta], direction: [xDir], velocity }) => {
              let gone = false
              if (!down && velocity > 1.5 && card.show) {
                gone = true
                props.swipeCards(card._id)
              }
              const x = calculateX(card._id, down, xDir, xDelta, gone)
              return (
                <Spring native to={{ x }} immediate={down} config={{ friction: 50, tension: 500 }}>
                  {({ x }) => (
                    <animated.div className='Cards__container' style={{ transform: x.interpolate(x => `translate3d(${x}px,0,0)`) }}>
                      <animated.div {...bind} id={card._id} className='Cards__item' style={{ backgroundImage: `linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7) ),url(${card.src})` }}>
                        <div className='Cards__item_container'>
                          <div className='Cards__item_title'>{decodeURIComponent(card.name)}</div>
                          <div className='Cards__item_subtitle'>{decodeURIComponent(card.description.slice(0, 256)) + (card.description.length >= 256 ? '...' : '')}</div>
                          <div className='Cards__item_bottom'>
                            <div className='Cards__item_icon' onClick={() => props.toggleCard(card._id)}>{card.likeState ? <Icon24Like /> : <Icon24LikeOutline />}</div>
                            <div className='Cards__item_icon'><span>{card.cost}</span><Icon24FavoriteOutline /></div>
                            <div className='Cards__item_icon' onClick={() => props.shareBtn(card._id)}><Icon24ShareOutline /></div>
                          </div>
                        </div>
                      </animated.div>
                    </animated.div>
                  )}
                </Spring>
              )
            }}
          </Gesture>)
    )
  return (
    <div>
      <div className='Cards'>
        <div className={props.loading ? 'Card__loading' : 'Card__loading Card__loading_hide'}>
          <div className='Cards__loading_item' />
        </div>
        {cardsElements}
        {props.empty ?
          <div className='Cards__container Cards__container_back'>
            <div className='Cards__item Cards__item_profile'>
              <div className="Cards__item_contents">
                <Placeholder
                  icon={<Icon56CheckCircleDeviceOutline />}
                  header="Задания закончились"
                  action={<Button size="l" onClick={() => props.clearHide()}>Начать сначала</Button>}
                >
                </Placeholder>
              </div>
            </div>
          </div>
          :
          <div className='Cards__container Cards__container_back'>
            <div className='Cards__item Cards__item_profile'>
              <div className="Cards__item_contents">
                <Spinner size="large" />
              </div>
            </div>
          </div>}
      </div>
    </div>
  )

}
export default Cards;
