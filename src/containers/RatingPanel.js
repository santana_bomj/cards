import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Spinner } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';
import * as ratingSelectors from "../store/rating/reducer";
import * as ratingActions from '../store/rating/actions';
import RatingList from './RatingList'

class RatingPanel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 'users'
        };
    }

    componentDidMount() {
        this.props.setData()
    }

    render() {
        return (
            <Panel id={this.props.id}>
                <PanelHeader>
                    Рейтинг
                </PanelHeader>
                {!this.props.loaded ?
                    <div className='Spinner__margin'><Spinner size="regular" /></div>
                    : <RatingList data={this.props.data} getUserOpen={this.props.getUserOpen} />
                }
                {this.props.snackbar}
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        loaded: ratingSelectors.getLoaded(state),
        snackbar: mainSelectors.getSnackbar(state),
        data: ratingSelectors.getData(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setData: () => dispatch(ratingActions.setData()),
        getUserOpen: (params) => dispatch(mainActions.getUserOpen(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RatingPanel);