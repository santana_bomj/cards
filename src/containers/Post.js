import React, { useState } from 'react';
import { Avatar } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import CommentIcon from './Icons/CommentIcon';
import ShareIcon from './Icons/ShareIcon';
import LikeIcon from './Icons/LikeIcon';
import CloseIcon from './Icons/CloseIcon'
import PenIcon from './Icons/PenIcon'
import MailIcon from './Icons/MailIcon'
import GroupIcon from './Icons/GroupIcon'

function Holdable({ id, onClick, onHold, children }) {

    const [timer, setTimer] = React.useState(null)

    function onPointerDown(evt) {
        const event = { ...evt } // convert synthetic event to real object
        const timeoutId = window.setTimeout(timesup.bind(null, event), 500)
        setTimer(timeoutId)
    }

    function onPointerUp(evt) {
        if (timer) {
            window.clearTimeout(timer)
            setTimer(null)
            onClick(evt)
        }
    }

    function timesup(evt) {
        setTimer(null)
        onHold(evt)
    }

    return (
        <div
            onPointerDown={onPointerDown}
            onPointerUp={onPointerUp}
            id={id}
        >
            {children}
        </div>
    )
}

function Post(props) {
    const [menu, setMenu] = useState(false);
    const [like, setLike] = useState(false);
    const [share, setShare] = useState(false);
    return (
        <div>
            <div className='Post'>
                <div className={menu ? 'Post__menu Post__menu_open' : 'Post__menu'}>
                    <div className='Post__menu_container'>
                        <CloseIcon />
                        <span>Убрать событие</span>
                    </div>
                </div>
                <div className='Post__menu-toggle' onClick={() => setMenu(!menu)}>
                </div>
                <div className='Post__header'>
                    <Avatar className='Post__header_avatar' size={32} src='http://i.mycdn.me/image?id=894865657106&t=32&plc=MOBILE&tkn=*ZodYL-owk2xV71plq3Fj34xI_gw' />
                    <div className='Post__header_content'>
                        <span>
                            <a>
                                Домовята - сообщество игры
                        </a>
                        </span>
                        <div className='Post__header_time'>
                            Пн 14:50
                    </div>
                    </div>
                </div>
                <div className='Post__content'>
                    <div className='Post__content_text'>
                        Подарки друзьям.<br />В этой теме Вы можете дарить ресурсы и бесплатные подарки своим соседям. А также просить друзей прислать подарок Вам.<br />ВНИМАНИЕ: ЗА ЛЮБОЙ ОБМАН МЕЖДУ ПОЛУЧАТЕЛЕМ И ДАРЯЩИМ АДМИНИСТРАЦИЯ ИГРЫ И МОДЕРАТОРЫ ГРУППЫ ОТВЕТСТВЕННОСТИ...
                    <a>ещё</a>
                    </div>
                    <div className='Post__content_photo'>
                        <img src="https://i.mycdn.me/image?id=874303046418&t=17&plc=MOBILE&tkn=*uHqUcA5aHifM6VVckeR5k6rLYtM" />
                    </div>
                </div>
                <div className='Post__footer'>
                    <div className='Post__footer_info'>
                        <span className='Post__icon'>
                            <img src='https://m.ok.ru/mres/img/react/w/ny2019.png' />
                            <span className='Post__icon_count'>5</span>
                        </span>
                        <span className='Post__icon'>
                            <CommentIcon fill='#666' />
                            <span className='Post__icon_count'>210</span>
                        </span>
                        <span className='Post__icon'>
                            <ShareIcon fill='#666' />
                            <span className='Post__icon_count'>5</span>
                        </span>
                    </div>
                    <div className='Post__footer_action'>
                        <span className="Post__action">
                            <CommentIcon fill='#666' />
                            <span>Комментарий</span>
                        </span>
                        <span className="Post__action" onClick={() => setShare(!share)}>
                            <ShareIcon fill='#666' />
                            <span>Поделиться</span>
                        </span>
                        {/* <div className='Post__share'>
                        <span className="Post__action" onClick={() => setShare(!share)}>
                            <ShareIcon fill='#666' />
                            <span>Поделиться</span>
                        </span>
                        <div className={share ? 'Post__share_container Post__share_open' : 'Post__share_container'}>
                            <div className='Post__share_close' onClick={() => setShare(false)}></div>
                            <div className='Post__share_item'>
                                <div className='Post__share_icon'>
                                    <ShareIcon fill='#999' />
                                </div>
                                <div>
                                    <div className='Post__share_title'>
                                        Поделиться сразу
                                    </div>
                                    <div className='Post__share_subtitle'>
                                        Друзья увидят заметку в ленте
                                    </div>
                                </div>
                            </div>
                            <div className='Post__share_item'>
                                <div className='Post__share_icon'>
                                    <PenIcon fill='#999' />
                                </div>
                                <div>
                                    <div className='Post__share_title'>
                                        Дополнить своим текстом
                                    </div>
                                </div>
                            </div>
                            <div className='Post__share_item'>
                                <div className='Post__share_icon'>
                                    <MailIcon fill='#999' />
                                </div>
                                <div>
                                    <div className='Post__share_title'>
                                        Отправить сообщением
                                    </div>
                                </div>
                            </div>
                            <div className='Post__share_item'>
                                <div className='Post__share_icon'>
                                    <GroupIcon fill='#999' />
                                </div>
                                <div>
                                    <div className='Post__share_title'>
                                        Опубликовать в группе
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                        <Holdable
                            onClick={() => console.log('click')}
                            onHold={() => setLike(!like)}
                        >
                            <span className="Post__action">
                                <LikeIcon fill='#666' />
                                <span>Класс</span>
                            </span>
                        </Holdable>
                    </div>
                </div>
                <div className={like ? 'Post__like Post__like_open' : 'Post__like'}>
                    <div className='Post__like_close' onClick={() => setLike(false)}></div>
                    <div className='Post__like_container'>
                        <div className='Post__like_item'>
                            1
                    </div>
                        <div className='Post__like_item'>
                            1
                    </div>
                    </div>
                </div>
            </div>
            {/* <div>
                <div className={share ? 'Post__share_close Post__share_open' : 'Post__share_close'}></div>
                <div className={share ? 'Post__share Post__share_open' : 'Post__share'} onClick={() => setShare(false)}>
                    <div className='Post__share_container'>
                        <div className='Post__share_item' onClick={() => console.log('+')}>
                            <div className='Post__share_icon'>
                                <ShareIcon fill='#999' />
                            </div>
                            <div>
                                Поделиться сейчас
                        </div>
                        </div>
                        <div className='Post__share_item'>
                            <div className='Post__share_icon'>
                                <PenIcon fill='#999' />
                            </div>
                            <div>
                                Дополнить своим текстом
                        </div>
                        </div>
                        <div className='Post__share_item'>
                            <div className='Post__share_icon'>
                                <GroupIcon fill='#999' />
                            </div>
                            <div>
                                Опубликовать в группе
                        </div>
                        </div>
                        <div className='Post__share_item'>
                            <div className='Post__share_icon'>
                                <MailIcon fill='#999' />
                            </div>
                            <div>
                                Отправить сообщением
                        </div>
                        </div>
                    </div>
                </div>
            </div> */}
        </div>
    )

}
export default Post;
