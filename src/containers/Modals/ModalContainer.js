import React from 'react';
import { connect } from 'react-redux';
import { ModalRoot, ModalCard } from '@vkontakte/vkui';
import * as mainSelectors from "../../store/main/reducer";
import * as mainActions from '../../store/main/actions';

import Icon56ErrorOutline from '@vkontakte/icons/dist/56/error_outline';
import Icon56CheckCircleOutline from '@vkontakte/icons/dist/56/check_circle_outline';
import Icon56ServicesOutline from '@vkontakte/icons/dist/56/services_outline';

class ModalContainer extends React.Component {
    componentDidUpdate() {
        if (this.props.activeModal)
            this.props.setPath()
    }
    render() {
        return (
            <ModalRoot activeModal={this.props.activeModal}>
                <ModalCard
                    id='share'
                    icon={<Icon56ServicesOutline />}
                    header="Поделиться"
                    caption=" "
                    onClose={() => this.props.modalBack()}
                    actions={([{
                        title: 'В истории',
                        mode: 'primary',
                        action: () => this.props.share()
                    }, {
                        title: 'Закрыть',
                        mode: 'secondary',
                        action: () => this.props.modalBack()
                    }])}
                    actionsLayout="vertical"
                />
                <ModalCard
                    id='error'
                    icon={<Icon56ErrorOutline />}
                    header="Ошибка"
                    caption={this.props.msg_error}
                    onClose={() => this.props.modalBack()}
                    actions={[{
                        title: 'Закрыть',
                        mode: 'primary',
                        action: () => this.props.modalBack()
                    }]}
                />
                <ModalCard
                    id='true'
                    icon={<Icon56CheckCircleOutline />}
                    header="Успешно"
                    caption={this.props.msg_error}
                    onClose={() => this.props.modalBack(2)}
                    actions={[{
                        title: 'Закрыть',
                        mode: 'primary',
                        action: () => this.props.modalBack(2)
                    }]}
                />
            </ModalRoot>
        )
    }
}

function mapStateToProps(state) {
    return {
        activeModal: mainSelectors.getActiveModal(state),
        msg_error: mainSelectors.getMsgError(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setActiveModal: (activeModal) => dispatch(mainActions.setActiveModal(activeModal)),
        modalBack: (value) => dispatch(mainActions.modalBack(value)),
        setPath: () => dispatch(mainActions.setPath()),
        share: () => dispatch(mainActions.share()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);