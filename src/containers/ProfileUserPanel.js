import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Gallery, Group, Cell, Header, Avatar, Spinner, Placeholder, PanelHeaderBack } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

import Icon56UsersOutline from '@vkontakte/icons/dist/56/users_outline';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';

import * as mainSelectors from "../store/main/reducer";

class ProfileUserPanel extends Component {
    render() {
        console.log(this.props)
        return (
            <Panel id={this.props.id}>
                <PanelHeader left={<PanelHeaderBack onClick={() => window.history.go(-1)} />}> {/* Со стартового экрана??? */}
                    {this.props.activeUser && this.props.activeUser.name ? this.props.activeUser.name : 'Профиль'}
                </PanelHeader>
                {this.props.activeUserLoading ?
                    <div className='Spinner__margin'><Spinner size="regular" /></div>
                    : (!this.props.activeUser ? <Placeholder
                        icon={<Icon56UsersOutline />}
                        stretched
                    >
                        Пользователь не найден
                    </Placeholder> :
                        <>
                            <Group>
                                <div className='Cards'>
                                    <div className='Cards__container'>
                                        <div className='Cards__item Cards__item_profile' id='Card__profile'>
                                            <div className="Cards__item_contents">
                                                <Avatar className="Cards__Avatar" src={this.props.activeUser.avatar} />
                                                <Cell className='Cell__center'>{this.props.activeUser.name}</Cell>
                                                <Cell indicator={<div className='Balance'><span>{this.props.activeUser.balance}</span><Icon24FavoriteOutline /></div>}>Баллы заданий</Cell>
                                                <Cell indicator={<div className='Balance'><span>{this.props.activeUser.authorRating}</span><Icon24FavoriteOutline /></div>}>Рейтинг автора</Cell>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Group>
                            {this.props.activeUser.performedCards.length > 0 &&
                                <Group header={<Header mode="secondary">Выполненые задания</Header>}>
                                    <Gallery
                                        className='Gallery__margin'
                                        slideWidth="100%"
                                        align="center"
                                        style={{ height: 400 }}
                                    >
                                        {this.props.activeUser.performedCards.map(card => {
                                            return <div className='Cards' key={card._id}>
                                                <div className='Cards__container'>
                                                    <div className='Cards__item' style={{ backgroundImage: `linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7) ),url(${card.src})` }} >
                                                        <div className='Cards__item_container'>
                                                            <div className='Cards__item_title'>{decodeURIComponent(card.name)}</div>
                                                            <div className='Cards__item_subtitle'>{decodeURIComponent(card.description.slice(0, 256)) + (card.description.length >= 256 ? '...' : '')}</div>
                                                            <div className='Cards__item_bottom'>
                                                                <div className='Cards__item_icon'><Icon24FavoriteOutline /></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        })}
                                    </Gallery>
                                </Group>}
                        </>)}
                {this.props.snackbar}
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        activeUserLoading: mainSelectors.getActiveUserLoading(state),
        activeUser: mainSelectors.getActiveUser(state),
        snackbar: mainSelectors.getSnackbar(state),
    };
}

export default connect(mapStateToProps)(ProfileUserPanel);
