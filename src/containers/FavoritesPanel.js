import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, PanelHeaderBack } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from "../store/main/reducer";
import * as shopSelectors from "../store/shop/reducer";
import * as favoritesActions from '../store/favorites/actions';
import * as favoritesSelectors from '../store/favorites/reducer';
import FavoritesList from './FavoritesList'

class FavoritesPanel extends Component {
    render() {
        return (
            <Panel id={this.props.id}>
                <PanelHeader left={<PanelHeaderBack onClick={() => window.history.go(-1)} />}>
                    Избранное
                </PanelHeader>
                <FavoritesList toggleCard={this.props.toggleCard} data={this.props.data} router={this.props.router} />
                {this.props.snackbar}
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        balance: mainSelectors.getBalance(state),
        isInit: shopSelectors.getIsInit(state),
        snackbar: mainSelectors.getSnackbar(state),
        data: favoritesSelectors.getFavorites(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        toggleCard: (id) => dispatch(favoritesActions.toggleCard(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesPanel);
