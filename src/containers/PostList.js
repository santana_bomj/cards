import React from 'react';
import { Avatar } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import Post from './Post';
import CommentIcon from './Icons/CommentIcon';
import ShareIcon from './Icons/ShareIcon';

function PostList(props) {
    let postList;
    let temp = [1, 2]
    postList = temp.map(post => {
        return <Post />
    })
    return (
       <>
        {postList}
       </>
    )

}
export default PostList;
