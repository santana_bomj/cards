import React, { useState } from 'react';
import { Spring, animated } from 'react-spring'
import { Gesture } from 'react-with-gesture'
import "./Cards.scss";

function calculateX(index, down, xDir, xDelta, gone) {
  if (gone) {
    return (200 + window.innerWidth) * (xDir < 0 ? -1 : 1)
  } else {
    return down ? xDelta : 0
  }
}

function StartCards(props) {
  const [gones] = useState(() => new Set())
  let cardsElements = null
  if (props.children !== undefined && props.children !== null)
    cardsElements = props.children.map(
      (card, index) =>
        !gones.has(index) && (
          <Gesture key={index}>
            {({ down, delta: [xDelta], direction: [xDir], velocity }) => {
              let gone = false
              if (!down && velocity > 1.5 && !gones.has(index)) {
                gone = true
                gones.add(index)
                if (gones.size === props.children.length)
                  setTimeout(() => props.goMain(), 333)
              }

              const x = calculateX(index, down, xDir, xDelta, gone)
              return (
                <Spring native to={{ x }} immediate={down} config={{ friction: 50, tension: 500 }}>
                  {({ x }) => (
                    <animated.div className='Cards__container' style={{ transform: x.interpolate(x => `translate3d(${x}px,0,0)`) }}>
                      <animated.div className='Cards__item'>
                        {card}
                      </animated.div>
                    </animated.div>
                  )}
                </Spring>
              )
            }}
          </Gesture>)
    )
  return (
    <div>
      <div className={!props.new ? 'Cards Cards__opacity' : 'Cards'}>
        {cardsElements}
      </div>
    </div>
  )

}
export default StartCards;
