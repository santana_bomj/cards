import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, Group, FixedLayout } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import StartCards from './StartCards'
import icon from '../img/icon.png'
import SwipeIcon from './Icons/SwipeIcon'
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';

class StartPanel extends Component {
    render() {
        return (
            <Panel id={this.props.id} separator={false}>
                <Group>
                    <div className={this.props.new ? 'Cards Cards__start Cards__opacity' : 'Cards Cards__start'}>
                        <div className='Cards__container'>
                            <div className='Cards__item Cards__item_start'>
                                <div className="Cards__item_contents">
                                    <img className="Cards__Avatar" src={icon} width='80' height='80' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <StartCards new={this.props.new} goMain={this.props.goMain}>
                        <div className='Start'>
                            <div className='Start__margin_bottom Start__margin_top'>
                                <div>Свайпайте карточки с заданиями</div>
                            </div>
                            <div className='Start__margin_bottom'>
                                Понравилось задание? Ставь <span>лайк</span>
                            </div>
                            <div className='Start__margin_bottom'>
                                Выполнили? Присылайте подтверждение и получайте <span>баллы</span>
                            </div>
                            <div>
                                Обменивайте <span>баллы</span> в магазине
                            </div>
                            <div className='Start__bottom'>
                                <SwipeIcon />
                            </div>
                        </div>
                        <div className='Start'>
                            <div className='Start__margin_top'>
                                <div>
                                    Пора разгрести
                                </div>
                                <div className='Start__bold'>Домашние дела</div>
                            </div>
                            <div>
                                <div>Мы Вам поможем</div>
                                <div><span>Сделайте свой первый свайп</span></div>
                            </div>
                            <div className='Start__bottom'>
                                <SwipeIcon />
                            </div>
                        </div>
                    </StartCards>
                </Group>
                <FixedLayout className='FiexedLayout__Start' vertical="bottom"><div className='Start__title'>Домашние дела</div></FixedLayout>
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        new: mainSelectors.getNew(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        goMain: () => dispatch(mainActions.goMain()),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartPanel);
