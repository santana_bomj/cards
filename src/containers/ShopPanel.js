import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Header, Group, Spinner } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from "../store/main/reducer";
import * as shopSelectors from "../store/shop/reducer";
import * as shopActions from '../store/shop/actions';
import ShopList from './ShopList'

import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';

class ShopPanel extends Component {
    render() {

        return (
            <Panel id={this.props.id}>
                <PanelHeader>
                    Магазин
                </PanelHeader>
                {this.props.balance !== null ? <Header mode="secondary" aside={<div className='Balance'><span>{this.props.balance}</span><Icon24FavoriteOutline /></div>}>Баланс</Header> : null}
                {!this.props.isInit ?
                    <Spinner size="regular" /> :
                    <Group className='Cell__indicator_full'>
                        <ShopList bought={this.props.bought} balance={this.props.balance} data={this.props.shop} buyShop={this.props.buyShop} />
                    </Group>
                }
                {this.props.snackbar}
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        balance: mainSelectors.getBalance(state),
        shop: shopSelectors.getShop(state),
        isInit: shopSelectors.getIsInit(state),
        snackbar: mainSelectors.getSnackbar(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        buyShop: (id, price) => dispatch(shopActions.buyShop(id, price)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopPanel);
