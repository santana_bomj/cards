import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Group, Header, FormStatus, Div, Button, Spinner } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import RatingList from './RatingList'

import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';
import * as authorSelectors from "../store/author/reducer";

class AuthorPanel extends Component {

    render() {
        console.log(this.props.author_loaded)
        return (
            <Panel id={this.props.id}>
                <PanelHeader>
                    Автор
                </PanelHeader>
                {this.props.author_loading ?
                    <div className='Spinner__margin'><Spinner size="regular" /></div>
                    : <>
                        <Div>
                            <FormStatus className='Margin-bottom'>Вы можете добавить собственное задание</FormStatus>

                            <Button size="xl" className='Button__margin' onClick={() => this.props.router.navigate('add')}>Добавить задание</Button>
                            {this.props.cards > 0 && <Button size="xl" mode="secondary" onClick={() => this.props.router.navigate('work')}>Мои задания</Button>}
                        </Div>
                        <Group header={<Header mode="secondary">Рейтинг авторов</Header>} >
                            <RatingList getUserOpen={this.props.getUserOpen} data={this.props.author_rating} />
                        </Group>
                    </>}
                {this.props.snackbar}
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        cards: mainSelectors.getCards(state),
        author_rating: authorSelectors.getAuthorRating(state),
        author_loading: authorSelectors.getAuthorLoading(state),
        snackbar: mainSelectors.getSnackbar(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getUserOpen: (params) => dispatch(mainActions.getUserOpen(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorPanel);

