import React, { Component } from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, Group, Button, Div } from "@vkontakte/vkui";
import "@vkontakte/vkui/dist/vkui.css";
import "./style.scss";
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';
import * as cardsSelectors from "../store/cards/reducer";
import * as cardsActions from '../store/cards/actions';

import Cards from "./Cards";

class MainPanel extends Component {
  render() {
    return (
      <Panel id={this.props.id}>
        <PanelHeader>Домашние дела</PanelHeader>
        <Group>
          <Cards shareBtn={this.props.shareBtn} clearHide={this.props.clearHide} empty={this.props.empty} cards={this.props.cards} swipeCards={this.props.swipeCards} toggleCard={this.props.toggleCard} />
        </Group>
        <Div>
          <Button size="xl" onClick={() => this.props.router.navigate('like')} >Избранное</Button>
        </Div>
        {this.props.snackbar}
      </Panel >
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: cardsSelectors.getData(state),
    snackbar: mainSelectors.getSnackbar(state),
    empty: cardsSelectors.getEmpty(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    swipeCards: (id) => dispatch(cardsActions.swipeCards(id)),
    toggleCard: (id) => dispatch(cardsActions.toggleCard(id)),
    clearHide: () => dispatch(cardsActions.clearHide()),
    shareBtn: (id) => dispatch(mainActions.shareBtn(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPanel);
