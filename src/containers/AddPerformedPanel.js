import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, PanelHeaderBack, Button, Div, FormStatus, CardScroll, Card, InfoRow } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import AnimateHeight from 'react-animate-height';
import PhotoCard from './PhotoCard'
import Icon36Add from '@vkontakte/icons/dist/36/add';

import * as vkSelectors from '../store/vk/reducer';
import * as vkActions from '../store/vk/actions';
import * as favoritesActions from '../store/favorites/actions';
import * as favoritesSelectors from '../store/favorites/reducer';

class AddPerformedPanel extends Component {
    render() {
        return (
            <Panel id={this.props.id}>
                <PanelHeader left={<PanelHeaderBack onClick={() => window.history.go(-1)} />}>
                    Подтвердить
                </PanelHeader>
                {Object.keys(this.props.active).length > 0 && <Div className='Profile__Div'>
                    <InfoRow header={this.props.active.name}>
                        {this.props.active.description}
                    </InfoRow>
                </Div>}
                <AnimateHeight
                    duration={500}
                    delay={100}
                    height={!this.props.tokenSettings.includes('photos') ? 'auto' : 0}
                >
                    <Div className='Profile__Div'>
                        <FormStatus>Для подтверждения выполнения, необходимо выдать разрешение на загрузку фото</FormStatus>
                        <Button size='xl' onClick={() => this.props.VKWebAppGetAuthToken('photos')}>
                            Выдать разрешение
                        </Button>
                    </Div>
                </AnimateHeight>
                <AnimateHeight
                    duration={500}
                    delay={100}
                    height={!this.props.selected && this.props.tokenSettings.includes('photos') ? 'auto' : 0}
                >
                    <FormStatus className='FormStatus__without-bottom'>Прикрепите фотографии, подтверждающие выполение</FormStatus>
                    <CardScroll>
                        <Card size="s" className='Card__file-icon'>
                            <input type='file' multiple accept="image/*" onChange={this.props.onSelectFile} />
                            <Icon36Add />
                        </Card>
                        {this.props.img.length > 0 &&
                            this.props.img.map((item, index) => {
                                return <Card size="s" key={index}>
                                    <PhotoCard img={item} removeFile={this.props.removeFile} />
                                </Card>
                            })}
                    </CardScroll>
                    <Div className='Div__without-top'>
                        <Button disabled={this.props.img.length === 0 ? true : false} size='xl' onClick={() => this.props.sendImage()}>
                            {this.props.active.sent ? 'Сохранить' : 'Отправить'}
                        </Button>
                    </Div>
                </AnimateHeight>
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        img: favoritesSelectors.getImg(state),
        active: favoritesSelectors.getActive(state),
        tokenSettings: vkSelectors.getTokenSettings(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onSelectFile: (e) => dispatch(favoritesActions.onSelectFile(e)),
        removeFile: (data) => dispatch(favoritesActions.removeFile(data)),
        sendImage: () => dispatch(favoritesActions.sendImage()),
        VKWebAppGetAuthToken: (scope) => dispatch(vkActions.VKWebAppGetAuthToken(scope))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddPerformedPanel);
