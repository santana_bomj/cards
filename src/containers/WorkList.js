import React from 'react';
import { Group, FormStatus, Div, Button, Alert, Placeholder } from '@vkontakte/vkui';
import Icon56FireOutline from '@vkontakte/icons/dist/56/fire_outline';
import Icon24LikeOutline from '@vkontakte/icons/dist/24/like_outline';
import Icon24ShareOutline from '@vkontakte/icons/dist/24/share_outline';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';

function WorkList(props) {
        let workElements = <Placeholder
                icon={<Icon56FireOutline />}
                stretched
        >
                У Вас нет заданий
      </Placeholder>
        if (props.user_cards.length > 0) {
                workElements = props.user_cards.map(item => {
                        return <Group key={item._id}>
                                <div className='FormStatus__margin'>
                                        {!item.moderated ? <FormStatus>
                                                Находится на модерации
                                                </FormStatus>
                                                :
                                                <FormStatus>
                                                        {item.message_out}
                                                </FormStatus>}
                                </div>
                                <div className='Cards'>
                                        <div className='Cards__container'>
                                                <div className='Cards__item' style={{ backgroundImage: `linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7) ),url(${item.src})` }}>
                                                        <div className='Cards__item_container'>
                                                                <div className='Cards__item_title'>{decodeURIComponent(item.name)}</div>
                                                                <div className='Cards__item_subtitle'>{decodeURIComponent(item.description.slice(0, 256)) + (item.description.length >= 256 ? '...' : '')}</div>
                                                                <div className='Cards__item_bottom'>
                                                                        <div className='Cards__item_icon'><Icon24LikeOutline /></div>
                                                                        <div className='Cards__item_icon'><span>{item.cost ? item.cost : ''}</span><Icon24FavoriteOutline /></div>
                                                                        <div className='Cards__item_icon'><Icon24ShareOutline /></div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <Div className='Div__without-padding-bottom'>
                                        <Button size='xl' mode='destructive' onClick={() => props.setPopout(<Alert
                                                actionsLayout="vertical"
                                                actions={[{
                                                        title: 'Удалить',
                                                        autoclose: true,
                                                        mode: 'destructive',
                                                        action: () => props.deleteCard(item._id),
                                                }, {
                                                        title: 'Отмена',
                                                        autoclose: true,
                                                        mode: 'cancel'
                                                }]}
                                                onClose={() => props.setPopout(null)}
                                        >
                                                <h2>Подтвердите действие</h2>
                                        </Alert>)}>Удалить</Button>
                                </Div>
                        </Group>
                })
        }
        return (
                <>
                        {workElements}
                        <div className='Padding-bottom' />
                </>
        )

}

export default WorkList;