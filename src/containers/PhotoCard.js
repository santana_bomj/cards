import React from 'react';
import Icon16Clear from '@vkontakte/icons/dist/16/clear';

function PhotoCard(props) {
    return (
        <div className='PhotoCard' style={{ backgroundImage: `url(${props.img})` }} >
            <div className='PhotoCard__close' onClick={() => props.removeFile(props.img)}><Icon16Clear /></div>
        </div>
    )

}
export default PhotoCard;
