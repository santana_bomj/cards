import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, PanelHeaderBack, File, Button, Div, FormStatus, FormLayout, Input, Textarea } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

import AnimateHeight from 'react-animate-height';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/lib/ReactCrop.scss';

import Icon24Camera from '@vkontakte/icons/dist/24/camera';
import Icon24LikeOutline from '@vkontakte/icons/dist/24/like_outline';
import Icon24ShareOutline from '@vkontakte/icons/dist/24/share_outline';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';

import * as mainSelectors from '../store/main/reducer';
import * as vkSelectors from '../store/vk/reducer';
import * as imgSelectors from '../store/img/reducer';
import * as imgActions from '../store/img/actions';
import * as vkActions from '../store/vk/actions';

class AddPanel extends Component {

    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop
            );
            this.props.onCropComplete(croppedImageUrl);
        }
    }

    getCroppedImg(image, crop) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        const cropWidth = crop.width * scaleX;
        const cropHeight = crop.height * scaleY;
        canvas.width = cropWidth;
        canvas.height = cropHeight;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            cropWidth,
            cropHeight,
            0,
            0,
            cropWidth,
            cropHeight
        );

        return new Promise((resolve, reject) => {
            const dataUrl = canvas.toDataURL("image/png");
            resolve(dataUrl);
        });
    }

    render() {
        return (
            <Panel id={this.props.id}>
                <PanelHeader left={<PanelHeaderBack onClick={() => window.history.go(-1)} />}>
                    Добавить задание
                </PanelHeader>
                <AnimateHeight
                    duration={500}
                    delay={100}
                    height={!this.props.tokenSettings.includes('photos') ? 'auto' : 0}
                >
                    <Div className='Profile__Div'>
                        <FormStatus>Для создания нового задания, необходимо выдать разрешение на загрузку фотографии, которая будет на фоне</FormStatus>
                        <Button size='xl' onClick={() => this.props.VKWebAppGetAuthToken('photos')}>
                            Выдать разрешение
                        </Button>
                    </Div>
                </AnimateHeight>
                <AnimateHeight
                    duration={500}
                    delay={100}
                    height={!this.props.selected && this.props.tokenSettings.includes('photos') ? 'auto' : 0}
                >
                    <Div className='Profile__Div'>
                        <File controlSize="xl" before={<Icon24Camera />} onChange={this.props.onSelectFile}>
                            Открыть галерею
                        </File>
                    </Div>
                    <AnimateHeight
                        duration={500}
                        delay={100}
                        height={this.props.file ? 'auto' : 0}
                    >
                        <Div className='Div__without-padding-bottom'>
                            <ReactCrop
                                src={this.props.file}
                                crop={this.props.crop}
                                onImageLoaded={this.onImageLoaded}
                                onComplete={this.onCropComplete}
                                onChange={this.props.onCropChange}
                            />
                        </Div>
                        <FormLayout>
                            <Input top='Название' name='name' value={this.props.name} onChange={this.props.handleInput} />
                            <Textarea top='Описание' name='description' value={this.props.description} onChange={this.props.handleInput} />
                        </FormLayout>
                        <Div>
                            <Button size="xl" onClick={() => this.props.toggleSelected()}>Предпросмотр</Button>
                        </Div>
                    </AnimateHeight>
                </AnimateHeight>
                <AnimateHeight
                    duration={500}
                    delay={100}
                    height={this.props.cropped_image && this.props.selected ? 'auto' : 0}
                >
                    <Div className='Div__without-padding-bottom'>
                        <FormStatus className='Margin-bottom'>Именно так будет выглядить Ваше задание</FormStatus>
                        <div className='Cards'>
                            <div className='Cards__container'>
                                <div className='Cards__item' style={{ backgroundImage: `linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7) ),url(${this.props.cropped_image})` }} >
                                    <div className='Cards__item_container'>
                                        <div className='Cards__item_title'>{decodeURIComponent(this.props.name)}</div>
                                        <div className='Cards__item_subtitle'>{decodeURIComponent(this.props.description.slice(0, 256)) + (this.props.description.length >= 256 ? '...' : '')}</div>
                                        <div className='Cards__item_bottom'>
                                            <div className='Cards__item_icon'><Icon24LikeOutline /></div>
                                            <div className='Cards__item_icon'><Icon24FavoriteOutline /></div>
                                            <div className='Cards__item_icon'><Icon24ShareOutline /></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Div>
                    <Div className='Div__without-padding-bottom'>
                        <Button size="xl" onClick={() => this.props.toggleSelected()}>Изменить</Button>
                    </Div>
                    <Div>
                        <Button size="xl" onClick={() => this.props.sendImage()}>Отправить</Button>
                    </Div>
                </AnimateHeight>
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        file: imgSelectors.getFile(state),
        crop: imgSelectors.getCrop(state),
        cropped_image: imgSelectors.getCroppedImage(state),
        selected: imgSelectors.getSelected(state),
        tokenSettings: vkSelectors.getTokenSettings(state),
        cards: mainSelectors.getCards(state),
        name: imgSelectors.getName(state),
        description: imgSelectors.getDescription(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onSelectFile: (e) => dispatch(imgActions.onSelectFile(e)),
        onCropChange: (crop, percentCrop) => dispatch(imgActions.onCropChange(crop, percentCrop)),
        onCropComplete: (crop) => dispatch(imgActions.onCropComplete(crop)),
        toggleSelected: () => dispatch(imgActions.toggleSelected()),
        sendImage: () => dispatch(imgActions.sendImage()),
        VKWebAppGetAuthToken: (scope) => dispatch(vkActions.VKWebAppGetAuthToken(scope)),
        handleInput: (e) => dispatch(imgActions.handleInput(e)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddPanel);
