import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Button, Div, Group, Cell, Avatar, Header, Gallery } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';

import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';

class ProfilePanel extends Component {
    render() {
        return (
            <Panel id={this.props.id}>
                <PanelHeader>
                    Профиль
                </PanelHeader>
                <Group>
                    <div className='Cards'>
                        <div className='Cards__container'>
                            <div className='Cards__item Cards__item_profile' id='Card__profile'>
                                <div className="Cards__item_contents">
                                    <Avatar className="Cards__Avatar" src={this.props.avatar} />
                                    <Cell className='Cell__center'>{this.props.name}</Cell>
                                    <Cell indicator={<div className='Balance'><span>{this.props.balance}</span><Icon24FavoriteOutline /></div>}>Баллы заданий</Cell>
                                    <Cell indicator={<div className='Balance'><span>{this.props.authorRating}</span><Icon24FavoriteOutline /></div>}>Рейтинг автора</Cell>
                                </div>
                            </div>
                        </div>
                    </div>

                    <Div>
                        <Button className='Button__margin' size="xl" onClick={() => this.props.shareBtn('profile')}>Поделиться</Button>
                    </Div>
                </Group>
                {this.props.performedCards.length > 0 &&
                    <Group header={<Header mode="secondary">Выполненые задания</Header>}>
                        <Gallery
                            className='Gallery__margin'
                            slideWidth="100%"
                            align="center"
                            style={{ height: 400 }}
                        >
                            {this.props.performedCards.map(card => {
                                return <div className='Cards' key={card._id}>
                                    <div className='Cards__container'>
                                        <div className='Cards__item' style={{ backgroundImage: `linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7) ),url(${card.src})` }} >
                                            <div className='Cards__item_container'>
                                                <div className='Cards__item_title'>{decodeURIComponent(card.name)}</div>
                                                <div className='Cards__item_subtitle'>{decodeURIComponent(card.description.slice(0, 256)) + (card.description.length >= 256 ? '...' : '')}</div>
                                                <div className='Cards__item_bottom'>
                                                    <div className='Cards__item_icon'><Icon24FavoriteOutline /></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            })}
                        </Gallery>
                    </Group>}
                {this.props.snackbar}
            </Panel >
        );
    }
}

function mapStateToProps(state) {
    return {
        balance: mainSelectors.getBalance(state),
        authorRating: mainSelectors.getAuthorRating(state),
        avatar: mainSelectors.getAvatar(state),
        snackbar: mainSelectors.getSnackbar(state),
        performedCards: mainSelectors.getPerformedCards(state),
        name: mainSelectors.getName(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        shareBtn: (from) => dispatch(mainActions.shareBtn(from)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePanel);
