import * as mainActions from './store/main/actions';
import * as favoritesActions from './store/favorites/actions';
import * as authorActions from './store/author/actions';
import * as shopActions from './store/shop/actions';

export default [
    { name: 'start', path: '/?s' },
    { name: 'main', path: '/main' },
    {
        name: 'profile', path: '/profile'
    },
    {
        name: 'profileuser', path: '/profile/:id', onActivate: dispatch => params => {
            dispatch(mainActions.getUser(params))
        }
    },
    {
        name: 'group', path: '/group'
    },
    {
        name: 'like', path: '/like', onActivate: dispatch => params => {
            dispatch(favoritesActions.openFavorites())
        }
    },
    {
        name: 'rating', path: '/rating'
    },
    {
        name: 'author', path: '/author', onActivate: dispatch => params => {
            dispatch(authorActions.openAuthor())
        }
    },
    { name: 'add', path: '/author/add' },
    {
        name: 'addPerformed', path: '/like/add', onActivate: dispatch => params => {
            dispatch(favoritesActions.openFavorite(params._id))
        }
    },
    {
        name: 'work', path: '/author/work', onActivate: dispatch => params => {
            dispatch(authorActions.openWork())
        }
    },
    {
        name: 'shop', path: '/shop', onActivate: dispatch => params => {
            dispatch(shopActions.openShop())
        }
    },
]